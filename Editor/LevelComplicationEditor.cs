﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Complication))]
public class LevelComplicationEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Keyboard), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float valuePadding = 35f;
        float padding = 0;

        float enumWidth = 90f;
        float enumPadding = enumWidth - valuePadding;

        // Calculate rects
        var startScore = new Rect(position.x + padding, position.y, valuePadding + 10, position.height);
        var hp = new Rect(position.x + valuePadding + padding, position.y, valuePadding - 5, position.height);
        var waveType = new Rect(position.x + valuePadding * 2 + padding, position.y, enumWidth, position.height);
        var count = new Rect(position.x + valuePadding * 3 + enumPadding + padding, position.y, valuePadding, position.height);
        var playerSpeed = new Rect(position.x + valuePadding * 4 + enumPadding + padding, position.y, valuePadding, position.height);
        var npcSpeed = new Rect(position.x + valuePadding * 5 + enumPadding + padding, position.y, valuePadding, position.height);
        var maxDistance = new Rect(position.x + valuePadding * 6 + enumPadding + padding, position.y, valuePadding, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(startScore, property.FindPropertyRelative("startScore"), GUIContent.none);
        EditorGUI.PropertyField(hp, property.FindPropertyRelative("hp"), GUIContent.none);
        EditorGUI.PropertyField(waveType, property.FindPropertyRelative("waveType"), GUIContent.none);
        EditorGUI.PropertyField(count, property.FindPropertyRelative("count"), GUIContent.none);
        EditorGUI.PropertyField(playerSpeed, property.FindPropertyRelative("playerSpeed"), GUIContent.none);
        EditorGUI.PropertyField(npcSpeed, property.FindPropertyRelative("npcSpeed"), GUIContent.none);
        EditorGUI.PropertyField(maxDistance, property.FindPropertyRelative("maxDistance"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlatformCreator))]
public class PlatformCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PlatformCreator platform = target as PlatformCreator;

        if (GUILayout.Button("Generate"))
        {
            platform.Generate();
        }

        if (GUILayout.Button("Clear"))
        {
            platform.Clear();
        }
    }

}

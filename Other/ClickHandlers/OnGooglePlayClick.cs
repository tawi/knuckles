﻿using UnityEngine;

public class OnGooglePlayClick : MonoBehaviour {

    private GoogleScoreManager googlePlay;
    
    private void Start()
    {
        googlePlay = FindObjectOfType<GoogleScoreManager>();
    }

    private void OnMouseDown()
    {
        googlePlay.OnSignClick();
    }
}

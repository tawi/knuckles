﻿using UnityEngine;

public class OnMusicClick : MonoBehaviour { 

    private void OnMouseDown()
    {
        SettingsController.Instance.OnMusicClick();
    }
}

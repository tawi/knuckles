﻿using UnityEngine;

public class OnScoreBoardClick : MonoBehaviour {

    private GoogleScoreManager googlePlay;
    
    private void Start()
    {
        googlePlay = FindObjectOfType<GoogleScoreManager>();
    }

    private void OnMouseDown()
    {
        googlePlay.OnScoreBoardClick();
    }
}

﻿using UnityEngine;

public class OnSoundClick : MonoBehaviour {

    private void OnMouseDown()
    {
        SettingsController.Instance.OnSoundClick();
    }
}

﻿using UnityEngine;

public class AdsSettings : MonoBehaviour
{
    public static readonly string androidAdsID = "1685182";
    public static readonly string iosAdsID = "1685183";
    public static readonly string rewardVideoID = "rewardedVideo";
    public static readonly string adMobID = "ca-app-pub-3940256099942544/6300978111";
    public static bool noAds = false;
    public static bool init = false;
    public static bool firstLauch = true;
}
﻿using UnityEngine;

public class ConstantsSettings : MonoBehaviour
{
    public static string GroundTag { get; private set; }
    public static string PlayerMask { get; private set; }
    public static string GroundMask { get; private set; }

    private void Awake()
    {
        GroundTag = "Ground";
        PlayerMask = "Player";
        GroundMask = "Ground";

        CheckLayer(GroundTag);
        CheckMask(PlayerMask);
        CheckMask(GroundMask);
    }

    private void CheckLayer(string tag)
    {
        var go = GameObject.FindGameObjectWithTag(tag);

        if (go == null)
        {
            Debug.LogError("Tag doesn't exist");
        }
    }

    private void CheckMask(string mask)
    {
        int layer = LayerMask.GetMask(mask);

        if (layer == 0 || string.IsNullOrEmpty(mask))
        {
            Debug.LogError("Mask doesn't exist");
        }
    }
}

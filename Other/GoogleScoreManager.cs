﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GoogleScoreManager : MonoBehaviour
{
    private static readonly string scoreBoard = "CgkIuoWEk8cKEAIQAA";

    void Start()
    {
        Init();
    }

    private void Init()
    {
#if UNITY_ANDROID
        //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
#endif

        if (PlayerPrefs.HasKey("Login"))
        {
            int value = PlayerPrefs.GetInt("Login");

            if (value == 1)
                OnSignClick();
        }
    }

    public void OnSignClick()
    {
#if UNITY_ANDROID
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success, string clbk) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt("Login", 1);
                }
                else
                {
                    PlayerPrefs.SetInt("Login", 0);
                }
                PlayerPrefs.Save();
            });
        }
#endif
    }

    public void OnScoreBoardClick()
    {
#if UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
        else
        {
            ScoreManager.Instance.SignInMessage();
        }
#endif
    }

    public static void AddScore(long value)
    {
#if UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(value, scoreBoard, null);
        }
#endif
    }
}

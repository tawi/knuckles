﻿using UnityEngine;

public class MusicManager : SingletonMonoBehaviour<MusicManager>
{
    [SerializeField]
    private AudioClip[] tracklist;

    private float currentTime;
    private float endTime;
    private int currentTrack = 0;

    private bool next = false;
    private bool isPlaying = false;

    private AudioSource source;

    protected override void Awake()
    {
        base.Awake();
        source = GetComponent<AudioSource>();
        source.ignoreListenerPause = true;
        MatchController_OnExitToMenu();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
        MatchController.Instance.OnExitToMenu += MatchController_OnExitToMenu;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
        MatchController.Instance.OnExitToMenu -= MatchController_OnExitToMenu;
    }

    private void Update()
    {
        if (isPlaying)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= endTime)
            {
                if (next)
                {
                    next = false;
                    currentTrack++;

                    if (currentTrack >= tracklist.Length)
                    {
                        currentTrack = 0;
                    }

                    currentTime = 0;
                    endTime = tracklist[currentTrack].length;
                    source.Stop();
                    source.clip = tracklist[currentTrack];
                    source.Play();
                }
                else
                {
                    currentTime = 0;
                }
            }
        }
    }

    public void Play()
    {
        isPlaying = true;
        source.Play();
    }

    public void Pause()
    {
        isPlaying = false;
        source.Pause();
    }

    public void Next()
    {
        next = true;
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (isStartGame)
        {
            Next();
        }
    }

    private void MatchController_OnExitToMenu()
    {
        currentTrack = 0;
        endTime = tracklist[0].length;
        source.Stop();
        source.clip = tracklist[0];
        if (isPlaying)
            source.Play();
        next = false;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : SingletonMonoBehaviour<SettingsController>
{
    [SerializeField]
    private Animator sound;
    [SerializeField]
    private Animator music;

    private const string state = "Switch";
    
    private static bool SoundValue = true;
    private static bool MusicValue = true;

    protected override void Awake()
    {
        base.Awake();

        InitSound();
        InitButtons();
    }

    public static void InitSound()
    {
        if (PlayerPrefs.HasKey("Sound"))
        {
            if (PlayerPrefs.GetInt("Sound") == 1)
            {
                SoundValue = true;
            }
            else
            {
                DisableSound();
            }

            if (PlayerPrefs.GetInt("Music") == 1)
            {
                MusicValue = true;
                EnableMusic();
            }
            else
            {
                DisableMusic();
            }
        }
        else
        {
            SoundValue = true;
            MusicValue = true;
            PlayerPrefs.SetInt("Sound", 1);
            PlayerPrefs.SetInt("Music", 1);
            EnableMusic();
        }
    }

    private void InitButtons()
    {       
        sound.SetBool(state, SoundValue);
        music.SetBool(state, MusicValue);
    }    

    public void OnSoundClick()
    {
        if (SoundValue)
        {
            DisableSound();
        }
        else
        {
            EnableSound();
        }

        sound.SetBool(state, SoundValue);
    }

    public void OnMusicClick()
    {
        if (MusicValue)
        {
            DisableMusic();
        }
        else
        {
            EnableMusic();
        }

        music.SetBool(state, MusicValue);
    }

    public static void EnableSound()
    {
        SoundValue = true;
        PlayerPrefs.SetInt("Sound", 1);
        AudioListener.pause = false;
    }

    public static void DisableSound()
    {
        SoundValue = false;
        PlayerPrefs.SetInt("Sound", 0);
        AudioListener.pause = true;
    }

    public static void EnableMusic()
    {
        MusicValue = true;
        PlayerPrefs.SetInt("Music", 1);

        MusicManager.Instance.Play();
     }

    public static void DisableMusic()
    {
        MusicValue = false;
        PlayerPrefs.SetInt("Music", 0);

        MusicManager.Instance.Pause();
    }

    public static bool GetSound()
    {
        return SoundValue;
    }

    public static bool GetMusic()
    {
        return MusicValue;
    }
}

﻿using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AdsController : SingletonMonoBehaviour<AdsController>
{
    private const float ADS_FREQUENCY = 4f;

    [SerializeField]
    private GameObject noAdsButton;

    private float playedGames = 0;

    private void Start()
    {
        var objects = FindObjectsOfType<AdsController>();

        if (objects.Length == 1)
        {
            Init();
        }
        else
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] != this)
                {
                    Destroy(objects[i].gameObject);
                }
            }

            Init();
        }
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
            MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    private void Init()
    {
        InitAds();
        StartCoroutine(WaitForInit());
        DontDestroyOnLoad(gameObject);
    }

    private void State(int state)
    {
        if (state >= 1)
        {
            if (AdsSettings.noAds == true)
            {
                DisableAdsButton();
            }
        }
        else
        {
            if (AdsSettings.firstLauch == true)
            {
                AdsSettings.firstLauch = false;
                StartCoroutine(WaitForInit());
            }
        }
    }

    private IEnumerator WaitForInit()
    {
        while (!AdsSettings.init)
        {
            yield return null;
        }

        if (!AdsSettings.noAds)
        {
         //   StartCoroutine(ShowAds());
        }
        else
        {
            DisableAdsButton();
        }
        yield return null;
    }

    private IEnumerator ShowAds()
    {
        ShowBannerAd();

        while (!Advertisement.IsReady())
        {
            yield return null;
        }

        Advertisement.Show();

        EnableAdsButton();
    }

    public void EnableAdsButton()
    {
        if (noAdsButton)
            noAdsButton.SetActive(true);
    }

    public void DisableAdsButton()
    {
        if (noAdsButton)
            noAdsButton.SetActive(false);
    }

    public void OnRewardClick(Action<bool> callback)
    {
        StartCoroutine(ShowRewardAds(callback));
    }

    private IEnumerator ShowRewardAds(Action<bool> callback)
    {
        while (!Advertisement.IsReady(AdsSettings.rewardVideoID))
        {
            yield return null;
        }

        Advertisement.Show(AdsSettings.rewardVideoID, new ShowOptions
        {
            resultCallback = result =>
            {
                if (result == ShowResult.Finished)
                {
                    callback(true);
                }
                else if (result == ShowResult.Failed)
                {
                    callback(false);
                }
            }
        }
        );
    }

    public void InitAds()
    {
        if (!Advertisement.isInitialized)
        {
#if UNITY_ANDROID || UNITY_EDITOR
            if (Advertisement.isSupported) Advertisement.Initialize(AdsSettings.androidAdsID, false);
#elif UNITY_IPHONE || UNITY_IPAD
                    if (Advertisement.isSupported) Advertisement.Initialize(Prototype.NetworkLobby.AdsSettings.iosAdsID, false);
#endif
        }
    }

    public void ShowBannerAd()
    {
        try
        {
        
        }
        catch (Exception ex)
        {
            GameObject.Find("DEBUG").GetComponent<Text>().text = ex.ToString();
        }
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        playedGames++;

        if (!AdsSettings.noAds)
        {
            if (playedGames % ADS_FREQUENCY == 0)
            {
                StartCoroutine(ShowAds());
            }
        }
    }
}


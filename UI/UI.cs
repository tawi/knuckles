﻿using UnityEngine;
using UnityEngine.UI;

public class UI : SingletonMonoBehaviour<UI>
{
    [SerializeField]
    private Text bulletMax, bulletCurrent;
    [SerializeField]
    private GameObject uiPanel;
    [SerializeField]
    private GameObject startZone;
    [SerializeField]
    private Transform hpPanel;

    private Image[] hps;

    private void Start()
    {
        hps = hpPanel.GetComponentsInChildren<Image>();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnExitToMenu += MatchController_OnExitToMenu;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
        {
            MatchController.Instance.OnExitToMenu -= MatchController_OnExitToMenu;
        }
    }

    public void SetMaxBullet(int val)
    {
        bulletMax.text = val.ToString();
    }

    public void SetCurrentBullet(int val)
    {
        bulletCurrent.text = val.ToString();
    }

    public void SetCurrentHealth(int val)
    {
        DisableHp();

        if (val > 0)
        {
            for (int i = 0; i < val; i++)
            {
                hps[i].gameObject.SetActive(true);
            }
        }
    }

    private void DisableHp()
    {
        for (int i = 0; i < hps.Length; i++)
        {
            hps[i].gameObject.SetActive(false);
        }
    }

    public void SwitchPanel(bool state)
    {
        uiPanel.SetActive(state);
        startZone.SetActive(!state);
    }

    public void Button_OnStartClick()
    {
        SwitchPanel(true);
        MatchController.Instance.Reset(true);
        CameraController.Instance.ShouldFollow = true;
    }

    private void MatchController_OnExitToMenu()
    {
        SwitchPanel(false);
    }
}

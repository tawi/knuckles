﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private const string SWITCH_STATE = "Switch";
    private const string ANIMATOR_PAUSE_KEY = "Pause";
    private const string ANIMATOR_CLOSE_KEY = "Close";
    private const string ANIMATOR_FINISH_KEY = "Finish";

    [SerializeField]
    private Animator music;
    [SerializeField]
    private Animator sound;
    [SerializeField]
    private GameObject continueButton, continueResetButton;
    [SerializeField]
    private Animator animatorMenu;

    private bool menuOpened = false;

    private PlayerHealth health;
    private AudioSource finishSource;

    private void Awake()
    {
        finishSource = GetComponent<AudioSource>();
        health = FindObjectOfType<PlayerHealth>();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
        MatchController.Instance.OnFinish += MatchController_OnFinish;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
        MatchController.Instance.OnFinish -= MatchController_OnFinish;
    }

    private void SwitchMusic()
    {
        music.SetBool(SWITCH_STATE, SettingsController.GetMusic());
    }

    private void SwitchSound()
    {
        sound.SetBool(SWITCH_STATE, SettingsController.GetSound());
    }

    public void EnableResetContinueButton()
    {
        continueResetButton.SetActive(true);
    }  

    public void Finish()
    {
        finishSource.Play();
    }

    public void Button_OnResumeClick()
    {
        menuOpened = false;
        Time.timeScale = 1f;
        animatorMenu.SetTrigger(ANIMATOR_CLOSE_KEY);
    }

    public void Button_OnPauseClick()
    {
        continueButton.SetActive(health.Alive);

        if (health.Alive)
        {
            if (!menuOpened)
            {
                menuOpened = true;
                Time.timeScale = 0f;
                continueResetButton.SetActive(false);
                animatorMenu.SetTrigger(ANIMATOR_PAUSE_KEY);
            }
        }
        else
        {
            animatorMenu.SetTrigger(ANIMATOR_FINISH_KEY);
        }

        SwitchMusic();
        SwitchSound();
    }

    public void Button_OnResetContinueClick()
    {
        Button_OnResumeClick();
        continueResetButton.SetActive(false);
        MatchController.Instance.OnContinueClick();
    }

    public void Button_OnResetClick()
    {
        Button_OnResumeClick();
        MatchController.Instance.Reset(false);
    }

    public void Button_OnExitClick()
    {
        Button_OnResumeClick();
        MatchController.Instance.ResetToMenu();
    }

    public void Button_OnMusicClick()
    {
        SettingsController.Instance.OnMusicClick();
        SwitchMusic();
    }

    public void Button_OnSoundClick()
    {
        SettingsController.Instance.OnSoundClick();
        SwitchSound();
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        //if (!isStartGame)
        //    Button_OnPauseClick();
    }

    private void MatchController_OnFinish()
    {      
        Button_OnPauseClick();
    }
}

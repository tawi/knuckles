﻿using UnityEngine;

public class FurniturePool : MonoBehaviour
{
    [SerializeField]
    private Sprite[] firnitureSprites;

    public Sprite GetRandomFurniture()
    {
        return firnitureSprites[Random.Range(0, firnitureSprites.Length)];
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelComplicationSettings")]
public class LevelComplicationSettings : ScriptableObject
{
    [SerializeField]
    private List<Complication> complication = new List<Complication>();

    public List<Complication> Complication
    {
        get
        {
            return complication;
        }
    }
}


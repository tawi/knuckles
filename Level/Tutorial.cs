﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Tutorial : SingletonMonoBehaviour<Tutorial>
{
    private const int LAST_STEP_NUMBER = 4;

    [SerializeField]
    private GameObject backGround;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Text[] descriptions;
    [SerializeField]
    private Text messageText;

    private Dictionary<int, string> texts = new Dictionary<int, string>()
    {
        { 0, "Here's tap zone for jump" },
        { 1, "Click here for knuckles shooting" },
        { 2, "Swipe for tank shooting" },
        { 3, "" },
        { 4, "Good Luck knuckles comrade! :)" }
    };

    private Dictionary<int, string> desctriptionsText = new Dictionary<int, string>()
    {
        { 0, "Balaika gives you additional score" },
        { 1, "Helmets gives you support knuckles" },
        { 2, "Tin of 'tushenka' gives you additional 1 hp" },
        { 3, "Shield gives you additional 2 armor points" },
    };

    public int Step { get; private set; } = -1;
    public bool FirstLaunch { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        if (!PlayerPrefs.HasKey("firstLaunch"))
        {
            PlayerPrefs.SetInt("firstLaunch", 1);
            PlayerPrefs.Save();
            FirstLaunch = true;
        }

        for (int i = 0; i < desctriptionsText.Count; i++)
        {
            descriptions[i].text = desctriptionsText[i];
        }
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    public void Activate()
    {
        if (FirstLaunch)
        {
            Invoke("NextStep", 2.8f);
        }
        else
        {
            animator.enabled = false;
        }
    }

    public void NextStep()
    {
        Time.timeScale = 1f;
        DisableBackground();
        if (Step != LAST_STEP_NUMBER)
            Step++;
        if (!IsInvoking("InvokeStep"))
        {
            if (Step != LAST_STEP_NUMBER)
                Invoke("InvokeStep", 0.2f);
            else
                Invoke("InvokeStep", 0.01f);
        }
    }

    private void InvokeStep()
    {
        Time.timeScale = 0f;

        animator.SetInteger("Step", Step);

        if (Step == LAST_STEP_NUMBER)
        {
            EndTutorial();
        }
        else
            SetText(Step);
    }

    private void DisableBackground()
    {
        backGround.SetActive(false);
    }

    public void EndTutorial()
    {
        Time.timeScale = 1f;
        FirstLaunch = false;
        DisableBackground();
        Step = -1;
    }

    public void SetText(int index)
    {
        messageText.text = texts[index];
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (isStartGame)
        {
            Activate();
        }
    }
}

﻿using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    private PowerUpType type;

    private float lifeTime = 12f;

    private AudioSource source;
    private Collider2D coll;
    private SpriteRenderer sprite;

    public PowerUpType Type
    {
        get
        {
            return type;
        }
    }

    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        coll = GetComponent<Collider2D>();
        source = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        lifeTime -= Time.fixedDeltaTime;

        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void Destroy()
    {
        if (source)
        {
            source.Play();
            Hide();
            Destroy(gameObject, source.clip.length + 0.1f);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Hide()
    {
        coll.enabled = false;
        sprite.enabled = false;
    }   
}

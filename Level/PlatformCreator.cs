﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlatformCreator : MonoBehaviour
{
    private const float FURNITURE_DISTANCE = 8f;

    [SerializeField]
    private GameObject platformPrefab;
    [SerializeField]
    private int platformLength = 3;
    [SerializeField]
    private Sprite[] furnitureSprites;

    private List<Sprite> existFurniture = new List<Sprite>();
    private int left;
    private int right;

    public Vector3 Left { get { return transform.position - LeftOffset; } }
    public Vector3 Right { get { return transform.position + RightOffset; } }

    public Vector3 LeftOffset
    {
        get
        {
            return new Vector3(left * Offset, 0, 0);
        }
    }

    public Vector3 RightOffset
    {
        get
        {
            return new Vector3(right * Offset, 0, 0);
        }
    }

    public int PlatformLength
    {
        get
        {
            return platformLength;
        }
        set
        {
            platformLength = value;
        }
    }

    public float Offset { get; private set; }

    public void Generate()
    {
        Offset = platformPrefab.GetComponent<SpriteRenderer>().bounds.size.x;

        Instantiate(platformPrefab, transform.position, Quaternion.identity, transform);

        left = Mathf.CeilToInt((float)(platformLength - 1) / 2);

        for (int i = 0; i < left; i++)
        {
            Instantiate(platformPrefab, transform.position - new Vector3(Offset * (i + 1), 0, 0), Quaternion.identity, transform);
        }

        right = platformLength - 1 - left;

        for (int i = 0; i < right; i++)
        {
            Instantiate(platformPrefab, transform.position + new Vector3(Offset * (i + 1), 0, 0), Quaternion.identity, transform);
        }

        GenerateBackground();
    }

    private void GenerateBackground()
    {
        existFurniture = furnitureSprites.ToList();
        InitFurniture(Left, -1);
        InitFurniture(Right, 1);

        float distance = Vector2.Distance(Right, Left);

        float countFloat = distance / FURNITURE_DISTANCE;

        int count = Mathf.FloorToInt(countFloat);

        float interval = distance / ((float)count + 1);

        for (int i = 0; i < count; i++)
        {
            InitFurniture(Left + new Vector3(interval * (i + 1), 0), 0);
        }
    }

    private void InitFurniture(Vector3 position, int side)
    {
        var prefabSprite = GetRandomFurniture();

        float yOffset = prefabSprite.bounds.extents.y;

        Vector3 startPosition = Vector3.zero;

        if (prefabSprite.bounds.extents.x < Offset / 2 && side != 1 && side != -1)
            startPosition = position + new Vector3(0, Offset / 2) + new Vector3(0, yOffset);
        else
            startPosition = position + new Vector3(0, Offset / 2) + new Vector3(0, yOffset) - new Vector3(prefabSprite.bounds.extents.x - Offset / 2, 0) * side;

        GameObject go = new GameObject("Furniture");
        SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
        renderer.sprite = prefabSprite;
        renderer.sortingOrder = -10;
        go.transform.position = startPosition;
        go.transform.parent = transform;
    }

    private Sprite GetRandomFurniture()
    {
        var sprite = existFurniture[Random.Range(0, existFurniture.Count)];
        existFurniture.Remove(sprite);
        return sprite;
    }

    public void Clear()
    {
        foreach (Transform platform in GetComponentsInChildren<Transform>())
        {
            if (platform != transform)
            {
                DestroyImmediate(platform.gameObject);
            }
        }
    }

    public void Initialize(GameObject platformPrefab, Sprite[] furnitureSprites)
    {
        this.platformPrefab = platformPrefab;
        this.furnitureSprites = furnitureSprites;
    }
}
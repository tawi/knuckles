﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NpcPool : MonoBehaviour
{
    [SerializeField]
    private GameObject[] prefabs;
    [SerializeField]
    private Transform pool;
    [SerializeField]
    private PoolType poolType;
    [SerializeField]
    private int maxNpc;
    [SerializeField]
    private Vector2 startPos;

    private List<GameObject> npcPool = new List<GameObject>();

    public enum PoolType { common, support };

    public PoolType NpcPoolType
    {
        get
        {
            return poolType;
        }
    }

    private void Start()
    {
        GeneratePool();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
        MatchController.Instance.OnContinueReset += MatchController_OnContinueReset;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
        MatchController.Instance.OnContinueReset -= MatchController_OnContinueReset;
    }

    private void GeneratePool()
    {
        int count = 0;

        for (int i = 0; i < prefabs.Length; i++)
        {
            for (int j = 0; j < maxNpc; j++)
            {
                var npc = Instantiate(prefabs[i], startPos + new Vector2(count * 10, 20), Quaternion.identity, pool);

                count++;
                npcPool.Add(npc);
                npc.gameObject.SetActive(false);
            }
        }

       // Invoke("HidePool", 0.2f);
    }

    public GameObject GetNpc()
    {
        var name = prefabs[Random.Range(0, prefabs.Length)].name;

        var npc = npcPool.FirstOrDefault(support => support.gameObject.activeSelf == false && support.gameObject.name.Contains(name));
        npc.gameObject.SetActive(true);

        return npc;
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        for (int i = 0; i < npcPool.Count; i++)
        {
            npcPool[i].gameObject.SetActive(false);
        }
    }

    private void MatchController_OnContinueReset()
    {
        MatchController_OnReset(false);
    }
}
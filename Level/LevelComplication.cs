﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelComplication : SingletonMonoBehaviour<LevelComplication>
{
    public enum WaveType { common, vadavarot }

    [SerializeField]
    private LevelComplicationSettings settings;

    private List<Complication> levels = new List<Complication>();

    protected override void Awake()
    {
        base.Awake();
        levels = settings.Complication;
    }

    public Complication GetComplication(int score)
    {
        return levels.LastOrDefault(level => level.StartScore <= score);
    }

    public Complication GetComplication(int score, WaveType type)
    {
        return levels.LastOrDefault(level => level.StartScore <= score && level.WaveType == type);
    }

    public int NextTarget(int score)
    {
        return levels.FirstOrDefault(level => level.StartScore > score).StartScore;
    }
}

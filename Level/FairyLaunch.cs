﻿using UnityEngine;

public class FairyLaunch : SingletonMonoBehaviour<FairyLaunch>
{
    private const float FINISH_OFFSET = 10f;
    private const float SPEED = 0.3f;
    private const float START_DELAY_TIME = 0.5f;
    private const string ATTACK_ANIMATOR_KEY = "Attack";

    [SerializeField]
    private Transform fairy;
    [SerializeField]
    private GameObject effectPrefab;
    [SerializeField]
    private GameObject effectHiding;
    [SerializeField]
    private AudioSource spellSource;

    private bool activated = false;
    private Vector3 startLocalPos;
    private Vector3 endPosition;
    private Vector2 spellPos;  

    private Animator animator;
    private GameObject effect;
    private AudioSource source;

    protected override void Awake()
    {
        base.Awake();

        animator = fairy.GetComponent<Animator>();
        startLocalPos = fairy.localPosition;
        source = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    public void FixedUpdate()
    {
        if (activated)
        {
            fairy.transform.localPosition = Vector3.MoveTowards(fairy.localPosition, endPosition, SPEED);

            if (Mathf.Abs(fairy.localPosition.y - endPosition.y) < 0.01f)
            {
                activated = false;
                animator.SetTrigger(ATTACK_ANIMATOR_KEY);
            }
        }
    }

    public void Activate(Vector2 position)
    {
        source.Play();
        fairy.localPosition = startLocalPos;
        endPosition = fairy.localPosition + new Vector3(0, -FINISH_OFFSET);
        spellPos = position + new Vector2(0, 2f);
        activated = true;
    }

    private void CreateEffect()
    {
        effect = Instantiate(effectPrefab, spellPos, Quaternion.identity, null);

        if (spellSource)
        {
            spellSource.Play();
        }

        Invoke("Reset", START_DELAY_TIME);
    }

    public void Reset()
    {
        Instantiate(effectHiding, fairy.position, Quaternion.identity, null);
        PlayerSpawner.Instance.Player.PlayerHealth.Reset();
        fairy.localPosition = startLocalPos;
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (!isStartGame)
        {
            Vector2 position = PlayerSpawner.Instance.Reset();
            Activate(position);
        }
    }
}

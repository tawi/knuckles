﻿using UnityEngine;

[System.Serializable]
public struct Complication
{
    [SerializeField]
    private int startScore;
    [SerializeField]
    private int hp;
    [SerializeField]
    private LevelComplication.WaveType waveType;
    [SerializeField]
    private int count;
    [SerializeField]
    private float playerSpeed;
    [SerializeField]
    private float npcSpeed;
    [SerializeField]
    private float maxDistance;

    public int StartScore { get { return startScore; } }
    public int Hp { get { return hp; } }
    public LevelComplication.WaveType WaveType { get { return waveType; } }
    public int Count { get { return count; } }
    public float PlayerSpeed { get { return playerSpeed; } }
    public float NpcSpeed { get { return npcSpeed; } }
    public float MaxDistance { get { return maxDistance; } }
}
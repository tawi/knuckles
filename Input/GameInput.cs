﻿using UnityEngine;

public class GameInput : MonoBehaviour
{
    [SerializeField]
    private RectTransform tapPanel;

    private const float SWIPE_TIME = 1f;
    private const float MINIMUM_SWIPE_DISTANCE = 10f;
    private const float minimumShotLimit = -25f;

    private float MAX_X_OFFSET;

    private Transform shotPoint;
    private IController controller;

    private void Awake()
    {
        MAX_X_OFFSET = tapPanel.rect.xMax * ((float)Screen.width / 1920) + (float)Screen.width * 0.07f;
        shotPoint = PlayerSpawner.Instance.Player.GetComponentInChildren<Anima2D.IkLimb2D>().transform;

        if (Application.isMobilePlatform)
        {
            controller = new MobileInput(shotPoint, MAX_X_OFFSET);
        }
        else
        {
            controller = new PcInput(shotPoint, minimumShotLimit, MAX_X_OFFSET);
        }
    }

    private void Update()
    {
        CheckJump();
        CheckClick();

        bool shot = controller.GetInput();
        ShotCannon(shot);
    }

    private void CheckJump()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetButtonDown("Jump"))
        {
            OnTapClick();
        }
#endif
    }

    private void CheckClick()
    {
        bool shot = false;

        if (CanShoot())
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (IsFarFromStart(position))
            {
                shotPoint.transform.position = position;
                shot = true;
            }
        }

        ShotPlayer(shot);
    }

    private bool CanShoot()
    {
        return Input.GetButton("Fire1") && MAX_X_OFFSET < Input.mousePosition.x;
    }

    private bool IsFarFromStart(Vector2 position)
    {
        return (position.x > minimumShotLimit);
    } 

    public void ShotPlayer(bool state)
    {
        PlayerSpawner.Instance.Player.Shoot(state);
    }

    public void ShotCannon(bool state)
    {
        PlayerSpawner.Instance.Player.ShootCannon(state);
    }

    public void OnTapClick()
    {
        PlayerSpawner.Instance.Player.Jump();
    }
}

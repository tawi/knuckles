﻿using UnityEngine;

public class MobileInput : IController
{
    private const float SWIPE_TIME = 1f;
    private const float MINIMUM_SWIPE_DISTANCE = 10f;

    private readonly Transform shotPoint;
    private readonly float maxXOffset;

    private Vector2 beginPosition, endPosition;
    private float previuousLockTime;

    public MobileInput(Transform shotPoint, float maxXOffset)
    {
        this.shotPoint = shotPoint;
        this.maxXOffset = maxXOffset;
    }

    public bool GetInput()
    {
        bool shot = false;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                beginPosition = Camera.main.ScreenToWorldPoint(touch.position);
                previuousLockTime = Time.time;
            }

            if (touch.phase == TouchPhase.Ended)
            {
                if (CheckLimit(touch.position))
                {
                    endPosition = Camera.main.ScreenToWorldPoint(touch.position);

                    if (endPosition.x > beginPosition.x)
                    {
                        float distance = Vector2.Distance(endPosition, beginPosition);

                        if (distance > MINIMUM_SWIPE_DISTANCE)
                        {
                            if (CanShot())
                            {
                                shotPoint.position = endPosition;
                                shot = true;
                            }
                        }
                    }
                }
            }
        }

        return shot;
    }

    private bool CheckLimit(Vector2 position)
    {
        return (position.x > maxXOffset);
    }

    private bool CanShot()
    {
        return Time.time < previuousLockTime + SWIPE_TIME;
    }
}

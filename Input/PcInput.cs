﻿using UnityEngine;

public class PcInput : IController
{
    private readonly Transform shotPoint;
    private readonly float minimumShotLimit;
    private readonly float maxXOffset;

    public PcInput(Transform shotPoint, float minimumShotLimit, float maxXOffset)
    {
        this.shotPoint = shotPoint;
        this.minimumShotLimit = minimumShotLimit;
        this.maxXOffset = maxXOffset;
    }

    public bool GetInput()
    {
        bool isShooting = false;

        if (Input.GetButton("Fire2") && maxXOffset < Input.mousePosition.x)
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (ClickAtTheProperPlace(position))
            {
                shotPoint.position = position;
                isShooting = true;
            }
        }

        return isShooting;
    }

    private bool ClickAtTheProperPlace(Vector2 position)
    {
        return (position.x > minimumShotLimit);
    }
}

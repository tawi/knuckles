﻿interface IBonus
{
    PowerUpType Type { get; }
    int Count { get; }
    void Apply();
}

public enum PowerUpType
{
    score,
    health,
    protection,
    ammo,
    support,
    ult
}
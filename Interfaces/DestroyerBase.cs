﻿using UnityEngine;

public abstract class DestroyerBase : MonoBehaviour
{
    private float max_Y_Offset = -11;

    private void FixedUpdate()
    {
        if (IsOutOfOffset() || IsDead())
        {
            Death();
        }
    }

    private bool IsOutOfOffset()
    {
        return transform.position.y < max_Y_Offset;
    }

    protected abstract void Death();
    protected abstract bool IsDead();
}

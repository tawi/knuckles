﻿using UnityEngine;

public abstract class EnemyBase : PlayerBase
{
    [SerializeField]
    protected EnemyType type;

    protected Ragdoll ragdoll;

    public EnemyType Type
    {
        get
        {
            return type;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        ragdoll = GetComponent<Ragdoll>();
    }

    public virtual void Initialize(float speed, int hp)
    {
        this.speed = speed;
        GetComponent<HealthBase>().MaxHp = hp;

        ragdoll.Reset();
    }

    protected Vector3 GetStartRaycastPoint(Vector3 offSet)
    {
        return transform.position + offSet;
    }
}

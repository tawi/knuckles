﻿using UnityEngine;

public abstract class PowerUpBase : MonoBehaviour, IBonus
{
    protected PowerUpHandler powerUpHandler;

    public int Count { get; protected set; }
    public PowerUpType Type { get; protected set; }

    protected virtual void Awake()
    {
        powerUpHandler = GetComponentInParent<PowerUpHandler>();
    }

    protected PowerUpBase(int newCount, PowerUpType newType)
    {
        Count = newCount;
        Type = newType;
    }

    public abstract void Apply();
}

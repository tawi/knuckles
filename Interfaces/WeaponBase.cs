﻿using UnityEngine;

public abstract class WeaponBase : MonoBehaviour
{
    [SerializeField]
    protected Transform projectile;
    [SerializeField]
    protected GameObject bulletPrefab;
    [SerializeField]
    protected int startBullets = 30;
    [SerializeField]
    protected float fireRate = 0.15f;
    [SerializeField]
    protected float realodTime = 1f;
    [SerializeField]
    protected WeaponType weaponType;
    [SerializeField]
    protected bool IsRotated = true;

    protected int bulletCount;
    protected float previousShotTime;
    protected float shootingTime;
    protected float delay = 0;

    protected IMuzzleFlash muzzleFlash;
    protected HealthBase health;
    protected WeaponSound weaponSound;
    protected Transform shootPoint;
    protected PlayerSound playerSound;

    public WeaponType WeaponType
    {
        get
        {
            return weaponType;
        }
    }

    protected virtual void Awake()
    {
        muzzleFlash = GetComponentInChildren<IMuzzleFlash>();
        shootPoint = PlayerSpawner.Instance.Player.GetComponentInChildren<Anima2D.IkLimb2D>().transform;

        health = GetComponentInParent<HealthBase>();
        playerSound = GetComponentInParent<PlayerSound>();
        weaponSound = GetComponent<WeaponSound>();
        Reset();
    }

    public virtual void Reset()
    {
        bulletCount = startBullets;
    }

    public void Shoot(bool isShooting)
    {
        if (!health.Alive)
            isShooting = false;

        if (isShooting)
        {
            if (bulletCount > 0)
            {
                muzzleFlash.Flash(isShooting);
                Shoot();
            }
            else
            {
                muzzleFlash.Flash(false);

                if (!IsRealoading())
                {
                    Reset();
                }
            }
        }
        else
        {
            muzzleFlash.Flash(isShooting);
        }
    }

    protected void Shoot()
    {
        if (CanShoot())
        {
            DecreaseBullets();

            previousShotTime = Time.time;

            var bullet = Instantiate(bulletPrefab, projectile.position, Quaternion.identity, null).GetComponent<BulletBase>();

            Vector3 direction = (shootPoint.position - projectile.position).normalized;

            ShotSound();
            bullet.SetDirection(direction);

            if (IsRotated)
                transform.rotation = Quaternion.FromToRotation(Vector2.right, direction);

            ShotAnimation();
        }
    }

    private bool IsRealoading()
    {
        return Time.time < previousShotTime + realodTime;
    }

    private bool CanShoot()
    {
        return Time.time >= previousShotTime + fireRate;
    }

    private void DecreaseBullets()
    {
        bulletCount--;

        UpdateUI();

        if (bulletCount == 0)
        {
            RealoadingSound();
            Invoke("OnRealodingFinished", realodTime + delay);
        }
    }

    protected abstract void RealoadingSound();
    protected abstract void ShotSound();

    protected virtual void UpdateUI() { }

    protected virtual void OnRealodingFinished() { }

    protected virtual void ShotAnimation() { } 
}

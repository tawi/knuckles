﻿using UnityEngine;

public abstract class PlayerBase : MonoBehaviour
{
    private const float JUMP_LOCK_TIME = 0.2f;

    [SerializeField]
    protected float jumpForce = 500;
    [SerializeField]
    protected float speed = 15f;
    [SerializeField]
    protected Transform groundCheck;
    [SerializeField]
    protected LayerMask ground;

    protected bool isGrounded = false;
    private float previousJumpTime;

    protected Rigidbody2D rb;
    protected HealthBase health;

    protected virtual void Awake()
    {
        health = GetComponent<HealthBase>();
        rb = GetComponent<Rigidbody2D>();
    }

    protected virtual void FixedUpdate()
    {
        if (health.Alive)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            CheckGround();
        }
    }

    protected virtual void CheckGround()
    {
        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, ground);
    }

    public virtual void Jump()
    {
        if (isGrounded && health.Alive && CanJump())
        {
            previousJumpTime = Time.time;
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            JumpSound();
        }
    }

    private bool CanJump()
    {
        return Time.time >= previousJumpTime + JUMP_LOCK_TIME;
    }

    protected virtual void JumpSound() { }
}

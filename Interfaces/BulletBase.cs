﻿using UnityEngine;

public abstract class BulletBase : MonoBehaviour
{
    [SerializeField]
    protected Rigidbody2D rb;
    [SerializeField]
    protected float vel = 50f;
    [SerializeField]
    protected float startDestroyTime = 1f;

    protected float destroyTime = 0;
    protected Vector3 direction;

    public float Speed
    {
        get
        {
            return vel;
        }
    }

    protected void FixedUpdate()
    {
        rb.velocity = direction * vel;

        destroyTime += Time.fixedDeltaTime;

        if (destroyTime > startDestroyTime)
        {
            Destroy(gameObject);
        }
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == ConstantsSettings.GroundTag)
        {
            Destroy(gameObject);
        }
    }

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
        transform.rotation = Quaternion.FromToRotation(Vector2.right, direction);
    }  
}

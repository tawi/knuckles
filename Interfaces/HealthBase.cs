﻿using UnityEngine;

public abstract class HealthBase : MonoBehaviour
{
    [SerializeField]
    protected int maxHp = 3;

    protected int hp;

    public bool Alive { get; set; }

    public int MaxHp
    {
        set
        {
            maxHp = value;
            Reset();
        }
    }

    protected virtual void Awake()
    {
        Reset();
    }

    public virtual bool TakeDamage(int count)
    {
        hp -= count;

        if (hp <= 0 && Alive)
        {
            Alive = false;
            hp = 0;
            Death();
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void AddHealth(int amount)
    {
        hp += amount;

        if (hp > maxHp)
        {
            hp = maxHp;
        }
    }

    public virtual void Reset()
    {
        hp = maxHp;
        Alive = true;
    }

    protected abstract void Death();
}

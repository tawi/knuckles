﻿using UnityEngine;

public class PlayerDestroyer : DestroyerBase
{
    private PlayerHealth health;

    private void Start()
    {
        health = GetComponent<PlayerHealth>();
    }

    protected override void Death()
    {
        if (health.Alive)
        {
            health.TakeDamage(999999);
            //matchController.Finish();
            //enabled = false;
        }
    }

    protected override bool IsDead()
    {
        return false;
    }
}

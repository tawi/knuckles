﻿using UnityEngine;

public class WeaponSound : MonoBehaviour
{
    [SerializeField]
    private AudioClip shotSound;

    private AudioSource weaponSource;

    private void Awake()
    {
        weaponSource = GetComponent<AudioSource>();
        weaponSource.clip = shotSound;
    }

    public void Shoot()
    {
        weaponSource.Play();
    }
}

﻿using UnityEngine;

public class SupportAim : MonoBehaviour
{
    [SerializeField]
    private Transform ik;

    private Transform playerIk;

    private void Awake()
    { 
        playerIk = PlayerSpawner.Instance.Player.GetComponentInChildren<Anima2D.IkLimb2D>().transform;
    }

    private void FixedUpdate()
    {
        ik.position = playerIk.position;
    }
}

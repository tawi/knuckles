﻿using UnityEngine;

public class SupportHealth : HealthBase
{
    private Ragdoll ragdoll;
    private EnemySound sound;
    private Support support;

    protected override void Awake()
    {
        base.Awake();
        ragdoll = GetComponent<Ragdoll>();
        sound = GetComponent<EnemySound>();
        support = GetComponent<Support>();
    }

    protected override void Death()
    {
        if (sound)
        {
            sound.Death();
        }

        if (ragdoll)
        {
            PlayerSpawner.Instance.Player.PlayerSupport.RemoveSupport(support);
            ragdoll.Activate();
        }
    }
}

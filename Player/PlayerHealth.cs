﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : HealthBase
{
    private PlayerRagdoll playerRagdoll;
    private PlayerSound playerSound;
    private Shield shield;

    public event Action OnDeath = delegate { };

    protected override void Awake()
    {
        shield = GetComponent<Shield>();
        playerRagdoll = GetComponent<PlayerRagdoll>();
        playerSound = GetComponent<PlayerSound>();
        //  Reset();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
            MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    public override bool TakeDamage(int amount)
    {
        if (shield.ShieldValue == 0)
        {
            base.TakeDamage(amount);
            UI.Instance.SetCurrentHealth(hp);
            if (amount == 1)
                playerSound.SayWounded();
        }
        else
        {
            shield.Damage(amount);
        }

        return shield.ShieldValue == 0;
    }

    public override void AddHealth(int amount)
    {
        base.AddHealth(amount);
        UI.Instance.SetCurrentHealth(hp);
    }

    protected override void Death()
    {
        OnDeath();
    }

    public override void Reset()
    {
        base.Reset();
        shield.DeActivate();
        UI.Instance.SetCurrentHealth(hp);
        playerRagdoll.Reset();
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (!isStartGame)
        {
            Alive = false;
        }
    }
}

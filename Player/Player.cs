﻿using UnityEngine;

public class Player : PlayerBase
{
    private const float START_SPEED = 10f;
    private const string ANIMATOR_GROUNDED_KEY = "IsGrounded";
    private const string ANIMATOR_VELOCITY_KEY = "Vel";

    [SerializeField]
    private Transform groundCheck2;

    private int nextTarget;

    private WeaponBase gunWeapon;
    private WeaponBase cannonWeapon;
    private PlayerDestroyer destroyer;
    private Transform lastGround;
    private PlayerSound playerSound;
    private Animator animator;

    public PlayerHealth PlayerHealth { get; private set; }
    public PlayerSupport PlayerSupport { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        destroyer = GetComponent<PlayerDestroyer>();
        animator = GetComponent<Animator>();
        gunWeapon = GetComponentInChildren<Weapon>();
        cannonWeapon = GetComponentInChildren<Cannon>();
        PlayerSupport = GetComponent<PlayerSupport>();
        playerSound = GetComponent<PlayerSound>();
        PlayerHealth = GetComponent<PlayerHealth>();

        nextTarget = LevelComplication.Instance.NextTarget(0);
        speed = START_SPEED;
    }

    protected override void CheckGround()
    {
        bool grounded = IsGrounded(groundCheck.position);

        if (!grounded)
            grounded = IsGrounded(groundCheck2.position);

        isGrounded = grounded;
    }

    private bool IsGrounded(Vector2 end)
    {
        return Physics2D.Linecast(transform.position, end, ground);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        animator.SetBool(ANIMATOR_GROUNDED_KEY, isGrounded);

        animator.SetFloat(ANIMATOR_VELOCITY_KEY, rb.velocity.y < -1 ? -1f : 1f);

        if (!health.Alive)
        {
            rb.velocity = new Vector2(0, 0);
        }

        if (ScoreManager.Instance.TotalScore > nextTarget)
        {
            speed = LevelComplication.Instance.GetComplication(nextTarget).PlayerSpeed;
            nextTarget = LevelComplication.Instance.NextTarget(nextTarget);
        }
    }

    public override void Jump()
    {
        if (CheckStep(0))
        {
            NextStep();
            base.Jump();
        }
    }

    public void Shoot(bool shouldShoot)
    {
        if ((Tutorial.Instance.Step == 1 && shouldShoot) || !Tutorial.Instance.FirstLaunch)
        {
            gunWeapon.Shoot(shouldShoot);
            PlayerSupport.Shoot(shouldShoot);

            NextStep();
        }
    }

    public void ShootCannon(bool shouldShoot)
    {
        if ((Tutorial.Instance.Step == 2 && shouldShoot) || !Tutorial.Instance.FirstLaunch)
        {
            cannonWeapon.Shoot(shouldShoot);
            NextStep();
        }
    }

    private bool CheckStep(int stepValue)
    {
        return (Tutorial.Instance.Step == stepValue || !Tutorial.Instance.FirstLaunch);
    }

    private void NextStep()
    {
        if (Tutorial.Instance.FirstLaunch)
            Tutorial.Instance.NextStep();
    }

    public void Reset()
    {
        PartialReset();

        speed = START_SPEED;
        nextTarget = LevelComplication.Instance.NextTarget(0);
    }

    public Vector2 ContinueReset()
    {
        PartialReset();

        PlatformCreator platform = lastGround.GetComponentInParent<PlatformCreator>();

        return transform.position = platform.Left + new Vector3(platform.Offset, GetComponent<Collider2D>().bounds.extents.y);
    }

    public void PartialReset()
    {
        gunWeapon.Reset();
        cannonWeapon.Reset();
        destroyer.enabled = true;
        PlayerSupport.Reset();
    }

    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == ConstantsSettings.GroundTag)
        {
            lastGround = collision.transform;
        }
    }

    protected override void JumpSound()
    {
        playerSound.Jump();
    }
}

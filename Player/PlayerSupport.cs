﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerSupport : MonoBehaviour
{
    private const int MAX_SUPPORT_COUNT = 2;
    private const string ANIMATOR_ABILITY_KEY = "Ability";

    [SerializeField]
    private GameObject supportPregab;
    [SerializeField]
    private Transform supportSpawn;

    private List<Support> supports = new List<Support>();
    private List<WeaponBase> supportWeaponList = new List<WeaponBase>();

    private PlayerSound playerSound;
    private Player player;
    private Animator animator;
    private NpcPool npcPool;

    private void Awake()
    {
        player = GetComponent<Player>();
        playerSound = GetComponent<PlayerSound>();
        animator = GetComponent<Animator>();
        npcPool = FindObjectsOfType<NpcPool>().Where(pool => pool.NpcPoolType == NpcPool.PoolType.support).FirstOrDefault();
    }

    public void Launch()
    {
        animator.SetBool(ANIMATOR_ABILITY_KEY, false);

        var npc = npcPool.GetNpc();

        var support = npc.GetComponent<Support>();
        support.transform.position = supportSpawn.position;
        AddWeaponSupport(support.Weapon);
        supports.Add(support);
        support.Reset();

        if (supports.Count > MAX_SUPPORT_COUNT)
        {
            if (supports[0] != null)
            {
                supports[0].CanJump = false;
                RemoveSupport(supports[0]);
            }
        }

        playerSound.SaySpawn();
    }

    public void Shoot(bool isShooting)
    {
        if (supportWeaponList.Count > 0)
        {
            for (int i = 0; i < supportWeaponList.Count; i++)
            {
                if (supportWeaponList[i] != null)
                {
                    supportWeaponList[i].Shoot(isShooting);
                }
                else
                {
                    supportWeaponList.Remove(supportWeaponList[i]);
                }
            }
        }
    }

    public void AddWeaponSupport(WeaponBase newSupport)
    {
        if (!supportWeaponList.Contains(newSupport))
        {
            supportWeaponList.Add(newSupport);
        }
    }

    public void RemoveSupport(Support support)
    {
        if (supports.Contains(support))
        {
            supportWeaponList.Remove(support.Weapon);
            supports.Remove(support);
        }
    }

    public void Reset()
    {
        animator.SetBool(ANIMATOR_ABILITY_KEY, false);
        supports.Clear();
        supportWeaponList.Clear();
    }
}

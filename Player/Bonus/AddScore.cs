﻿using UnityEngine;

public class AddScore : PowerUpBase
{
    public AddScore() : base(BonusSettings.AddBonusCount, PowerUpType.score) { }

    public override void Apply()
    {
        ScoreManager.Instance.AddScore(Count);
    }
}

﻿using UnityEngine;

public class AddProtection : PowerUpBase
{
    public AddProtection(Shield shield) : base(BonusSettings.AddShieldCount, PowerUpType.protection)
    {
        this.shield = shield;
    }

    private Shield shield;

    public override void Apply()
    {
        shield.Activate();
    }
}

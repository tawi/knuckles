﻿using UnityEngine;

public class AddHealth : PowerUpBase
{
    public AddHealth(HealthBase health) : base(BonusSettings.AddHealthCount, PowerUpType.health)
    {
        this.health = health;
    }

    private HealthBase health;

    public override void Apply()
    {
        health.AddHealth(Count);
    }
}

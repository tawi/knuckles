﻿using UnityEngine;

public class AddSupport : PowerUpBase
{
    private Animator animator;

    public AddSupport(Animator animator) : base(0, PowerUpType.support)
    {
        this.animator = animator;
    }

    public override void Apply()
    {
        animator.SetBool("Ability", true);
        animator.SetTrigger("Luke");
    }
}

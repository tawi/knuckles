﻿using UnityEngine;

public class AddAmmo : PowerUpBase
{
    public AddAmmo(Weapon weapon) : base(BonusSettings.AddAmmoCount, PowerUpType.ammo)
    {
        this.weapon = weapon;
    }

    private Weapon weapon;

    public override void Apply()
    {
        //weapon.ShootingTime = Count;
        //weapon.Shooting = true;  
    }
}

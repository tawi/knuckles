﻿using System.Collections.Generic;
using UnityEngine;

public class PowerUpHandler : MonoBehaviour
{
    private Dictionary<PowerUpType, IBonus> powerUps = new Dictionary<PowerUpType, IBonus>();

    private void Awake()
    {
        powerUps.Add(PowerUpType.health, new AddHealth(GetComponent<PlayerHealth>()));
        powerUps.Add(PowerUpType.score, new AddScore());
        powerUps.Add(PowerUpType.protection, new AddProtection(GetComponent<Shield>()));
        powerUps.Add(PowerUpType.ammo, new AddAmmo(GetComponentInChildren<Weapon>()));
        powerUps.Add(PowerUpType.support, new AddSupport(GetComponent<Animator>()));
    }

    public void TakeBonus(PowerUpType type)
    {
        if (powerUps.ContainsKey(type))
        {
            powerUps[type].Apply();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var powerUp = collision.GetComponent<PowerUp>();

        if (powerUp != null)
        {
            TakeBonus(powerUp.Type);
            powerUp.Destroy();
        }
    }
}
﻿public static class BonusSettings
{
    public const int AddHealthCount = 1;
    public const int AddAmmoCount = 2;
    public const int AddBonusCount = 3;
    public const int AddShieldCount = 2;
}


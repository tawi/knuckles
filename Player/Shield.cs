﻿using UnityEngine;

public class Shield : MonoBehaviour
{
    private const float SHIELD_TRANSPARENCY_RATE = 0.4f;

    [SerializeField]
    protected int maxShield = 2;
    [SerializeField]
    private GameObject sphere;

    private SpriteRenderer[] layers;

    public int ShieldValue { get; private set; }

    private void Awake()
    {
        layers = sphere.GetComponentsInChildren<SpriteRenderer>();
    }

    public void Damage(int count)
    {
        ShieldValue -= count;

        if (ShieldValue < 0)
        {
            ShieldValue = 0;
        }

        DamageSphere();
    }

    private void DamageSphere()
    {
        if (ShieldValue == 1)
        {
            for (int i = 0; i < layers.Length; i++)
            {
                layers[i].color = new Color32(255, 255, 255, (byte)(255 * SHIELD_TRANSPARENCY_RATE));
            }
        }
        else if (ShieldValue == 0)
        {
            sphere.SetActive(false);
        }
    }

    public void Activate()
    {
        sphere.SetActive(true);
        ShieldValue = maxShield;

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i].color = new Color32(255, 255, 255, 255);
        }
    }

    public void DeActivate()
    {
        sphere.SetActive(false);
        ShieldValue = 0;
    }  
}

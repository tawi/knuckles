﻿using UnityEngine;

public class SupportDestroyer : DestroyerBase
{
    private const float MAX_X_OFFSET = -35f;

    private Ragdoll ragdoll;
    private Support support;

    private void Awake()
    {
        support = GetComponent<Support>();
        ragdoll = GetComponent<Ragdoll>();
    }

    protected override void Death()
    {
        PlayerSpawner.Instance.Player.PlayerSupport.RemoveSupport(support);
        ragdoll.SwitchRagdollState(false);
        gameObject.SetActive(false);
    }

    protected override bool IsDead()
    {
        return transform.position.x <= (PlayerSpawner.Instance.Player.transform.position.x + MAX_X_OFFSET);
    }
}

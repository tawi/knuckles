﻿using UnityEngine;

public class Track : PlayerBase
{
    protected override void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    protected override void FixedUpdate()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    public void Initialize(float speed, Vector2 velocity)
    {
        Awake();
        this.speed = speed;
        rb.velocity = velocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var enemy = collision.transform.GetComponent<EnemyBase>();

        if (enemy != null)
        {
            collision.transform.GetComponent<HealthBase>().TakeDamage(9999);
        }
    }
}

﻿using UnityEngine;

public class Weapon : WeaponBase
{
    [SerializeField]
    private bool updateUI = false;

    public override void Reset()
    {
        base.Reset();

        if (updateUI)
        {
            UI.Instance.SetMaxBullet(startBullets);
            UI.Instance.SetCurrentBullet(startBullets);
        }
    }

    protected override void RealoadingSound()
    {
        if (playerSound)
        {
            playerSound.Reload();
            playerSound.SayReloading();
        }
    }

    protected override void UpdateUI()
    {
        if (updateUI)
        {
            UI.Instance.SetCurrentBullet(bulletCount);
        }
    }

    protected override void ShotSound()
    {
        weaponSound.Shoot();
    }

    protected override void OnRealodingFinished()
    {
        UpdateUI();
    }
}

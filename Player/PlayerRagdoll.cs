﻿using UnityEngine;

public class PlayerRagdoll : MonoBehaviour
{
    [SerializeField]
    private GameObject trackPrefab;
    [SerializeField]
    private GameObject towerPrefab;
    [SerializeField]
    private GameObject tower;
    [SerializeField]
    private GameObject explodeEffect;

    private Track trackPart;
    private Ragdoll towerPart;
    private SkinnedMeshRenderer[] skinnedMeshes;
    private MeshRenderer[] meshes;
    private SpriteRenderer[] sprites;
    private Rigidbody2D rb;
    private Player player;
    private PlayerSound sound;

    private void Awake()
    {
        skinnedMeshes = GetComponentsInChildren<SkinnedMeshRenderer>();
        meshes = GetComponentsInChildren<MeshRenderer>();
        sprites = GetComponentsInChildren<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        player = GetComponent<Player>();
        sound = GetComponent<PlayerSound>();
        PlayerHealth playerHealth = GetComponent<PlayerHealth>();
        playerHealth.OnDeath += PlayerHealth_OnDeath;

        trackPart = Instantiate(trackPrefab, transform.position, Quaternion.identity, null).GetComponent<Track>();
        trackPart.gameObject.SetActive(false);
        towerPart = Instantiate(towerPrefab, tower.transform.position, tower.transform.rotation, null).GetComponent<Ragdoll>();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
            MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    public void SwitchState(bool isEnabled)
    {
        //Debug.Log("I'M HERE MOFOOOOOOOOOOOOO");
        if (meshes?.Length > 0)
        {
            for (int i = 0; i < meshes.Length; i++)
            {
                meshes[i].enabled = isEnabled;
            }

            for (int i = 0; i < skinnedMeshes.Length; i++)
            {
                skinnedMeshes[i].enabled = isEnabled;
            }

            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].enabled = isEnabled;
            }

            rb.simulated = isEnabled;

            if (!isEnabled)
            {
                trackPart.gameObject.SetActive(true);
                towerPart.gameObject.SetActive(true);
            }

            tower.gameObject.SetActive(isEnabled);
        }
    }

    public void Reset()
    {
        SwitchState(true);
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        trackPart.gameObject.SetActive(false);
        towerPart.gameObject.SetActive(false);

        if (!isStartGame)
        {
            SwitchState(false);
        }
    }

    private void PlayerHealth_OnDeath()
    {
        SwitchState(false);

        trackPart.transform.position = transform.position;
        towerPart.transform.position = transform.position + new Vector3(2.5f, 1.5f);

        trackPart.Initialize(4, rb.velocity);
        towerPart.Activate();
        Instantiate(explodeEffect, towerPart.transform.position, Quaternion.identity, null);

        sound.Explode();
    }
}

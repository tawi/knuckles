﻿using UnityEngine;

public class Cannon : WeaponBase
{
    private const string ANIMATOR_SHOOTING_KEY = "Shoot";
    private const string ANIMATOR_REALODING_KEY = "Realoding";
    private const string ANIMATOR_OPEN_LUKE_TRIGGER_KEY = "Luke";

    [SerializeField]
    private AnimationClip[] clips;

    private Animator animator;

    protected override void Awake()
    {
        base.Awake();
        animator = GetComponentInParent<Animator>();

        for (int i = 0; i < clips.Length; i++)
        {
            delay -= clips[i].length;
        }
    }

    protected override void ShotAnimation()
    {
        animator.SetTrigger(ANIMATOR_SHOOTING_KEY);
        animator.SetBool(ANIMATOR_REALODING_KEY, true);
        animator.SetTrigger(ANIMATOR_OPEN_LUKE_TRIGGER_KEY);
    }

    protected override void OnRealodingFinished()
    {
        animator.SetBool(ANIMATOR_REALODING_KEY, false);
    }

    protected override void RealoadingSound()
    {

    }

    protected override void ShotSound()
    {
        weaponSound.Shoot();
    }
}

﻿using UnityEngine;

public class AKMuzzleFlash : MonoBehaviour, IMuzzleFlash
{
    private const string ANIMATOR_SHOOTING_KEY = "Shooting";

    private Animator muzzleFlash;

    private void Start()
    {
        muzzleFlash = GetComponentInChildren<Animator>();
    }

    public void Flash(bool isEnabled)
    {
        if (muzzleFlash)
            muzzleFlash.SetBool(ANIMATOR_SHOOTING_KEY, isEnabled);
    }
}

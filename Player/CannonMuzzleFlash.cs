﻿using UnityEngine;

public class CannonMuzzleFlash : MonoBehaviour, IMuzzleFlash
{
    private const string ANIMATOR_SHOOTING_KEY = "Shoot";

    private Animator muzzleFlash;

    private void Awake()
    {
        muzzleFlash = GetComponentInChildren<Animator>();
    }

    public void Flash(bool isEnabled)
    {
        if (isEnabled)
            muzzleFlash.SetTrigger(ANIMATOR_SHOOTING_KEY);
    }
}

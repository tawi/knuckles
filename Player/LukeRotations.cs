﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LukeRotations : MonoBehaviour
{
    private Quaternion startRot;

    private void Update()
    {
        startRot = transform.localRotation;
    }

    void LateUpdate()
    {
        transform.localRotation = startRot;
    }
}

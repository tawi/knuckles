﻿using UnityEngine;

public class BulletController : BulletBase
{
    [SerializeField]
    private int damageCount = 1;
    [SerializeField]
    private bool destroyAfterHit = true;

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);

        var enemy = collision.transform.GetComponent<EnemyBase>();

        if (enemy != null)
        {
            var isDead = collision.transform.GetComponent<HealthBase>().TakeDamage(damageCount);

            if (isDead)
            {
                ScoreManager.Instance.AddScore(ScoreManager.DEATH_SCORE);
            }

            if (destroyAfterHit)
                Destroy(gameObject);
        }
        else
        {
            var spell = collision.transform.GetComponent<SpellController>();

            if (spell != null)
            {
                Destroy(spell.gameObject);
                if (destroyAfterHit)
                    Destroy(gameObject);
            }
        }
    }
}

﻿using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    private const float TANK_IDLE_PITCH = 0.4f;
    private const string ANIMATOR_TALKING_KEY = "Talking";

    [SerializeField]
    private AudioSource jumpSource;
    [SerializeField]
    private AudioSource reloadSource;
    [SerializeField]
    private AudioSource tankSource;
    [SerializeField]
    private AudioClip[] realodingPhrase;
    [SerializeField]
    private AudioClip[] spawnPhrase;
    [SerializeField]
    private AudioSource spawnSource;
    [SerializeField]
    private AudioSource explodeSource;
    [SerializeField]
    private AudioClip woundedPhrase, dounoudewaiPhrase;

    private AudioSource audioSource;
    private Animator animator;
    private HealthBase health;  

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        health = GetComponent<HealthBase>();
    }

    private void FixedUpdate()
    {
        if (health.Alive || tankSource.pitch == TANK_IDLE_PITCH)
        {
            if (!tankSource.isPlaying)
                tankSource.Play();
        }
        else
        {
            if (tankSource.isPlaying)
                tankSource.Stop();
        }
    }

    public void Jump()
    {
        jumpSource.Play();
    }

    public void Reload()
    {
        reloadSource.Play();
    }

    public void SayReloading()
    {
        Say(realodingPhrase[Random.Range(0, realodingPhrase.Length)]);
    }

    public void SayWounded()
    {
        Say(woundedPhrase);
    }

    public void SaySpawn()
    {
        spawnSource.clip = spawnPhrase[Random.Range(0, spawnPhrase.Length)];
        spawnSource.Play();
    }

    public void SayDewayPhrase()
    {
        Say(dounoudewaiPhrase);
    }

    public void Say(AudioClip phrase)
    {
        //  if (!source.isPlaying)

        animator.SetBool(ANIMATOR_TALKING_KEY, true);

        float talkTime = phrase.length;

        audioSource.Stop();
        audioSource.clip = phrase;
        audioSource.Play();

        if (IsInvoking("StopTalking"))
        {
            CancelInvoke("StopTalking");
        }

        Invoke("StopTalking", talkTime);
    }

    public void Explode()
    {
        explodeSource.Play();
    }

    public void IdleTank(bool isEnabled)
    {
        if (isEnabled)
        {
            tankSource.pitch = TANK_IDLE_PITCH;
        }
        else
        {
            tankSource.pitch = 1f;
        }
    }

    private void StopTalking()
    {
        animator.SetBool(ANIMATOR_TALKING_KEY, false);
    }
}

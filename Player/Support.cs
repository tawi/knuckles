﻿using UnityEngine;

public class Support : EnemyGround
{
    private const float START_LIFE_TIME = 12f;
    private const string ANIMATOR_GROUNDED_KEY = "IsGrounded";
    private const string ANIMATOR_TRIGGER_STOP_FLIP_KEY = "StopFlip";

    private bool running = false;
    private float lifeTime;

    private Animator animator;
    private AudioSource source;

    public bool CanJump { get; set; }
    public WeaponBase Weapon { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        Weapon = GetComponentInChildren<WeaponBase>();
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        startRaycastPoint = new Vector3(shift, -1);
        rayCastDirection = new Vector3(0f, -4f);
        force = 300f;
    }

    protected override void FixedUpdate()
    {
        if (running)
        {
            base.FixedUpdate();
            speed = PlayerSpawner.Instance.Player.Speed;
            animator.SetBool(ANIMATOR_GROUNDED_KEY, isGrounded);

            lifeTime -= Time.fixedDeltaTime;

            if (lifeTime <= 0)
            {
                CanJump = false;
            }
        }

        if (!health.Alive)
        {
            source.Stop();
        }
    }

    public void Reset()
    {
        health.Reset();
        ragdoll.Reset();
        animator.Play(0);
        isGrounded = true;
        lifeTime = START_LIFE_TIME;
        running = false;
        CanJump = true;

        rb.AddForce(GetSpawnForce(), ForceMode2D.Impulse);
    }

    private Vector3 GetSpawnForce()
    {
        return Quaternion.Euler(0, 0, -40) * transform.up * force;
    }

    private void StopFlip()
    {
        running = true;
        animator.SetTrigger(ANIMATOR_TRIGGER_STOP_FLIP_KEY);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == ConstantsSettings.GroundTag)
        {
            if (!running)
            {
                StopFlip();
            }
        }
    }

    protected override void CheckPlayerHit()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    protected override bool ShouldJump()
    {
        return (!IsGrounded() && CanJump);
    }

    protected override bool IsGrounded()
    {
        return Physics2D.Raycast(GetStartRaycastPoint(startRaycastPoint), rayCastDirection, groundLayer);
    }
}

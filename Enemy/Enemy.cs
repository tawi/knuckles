﻿using UnityEngine;

public class Enemy : EnemyGround
{
    protected const float MINIMUM_SPEED_TO_JUMP = 10f;

    protected override void Awake()
    {
        base.Awake();
        startRaycastPoint = new Vector3(-shift, 0);
        rayCastDirection = new Vector3(0f, -4f);
    }

    protected override bool ShouldJump()
    {
        bool jump = false;

        if (!IsGrounded())
        {
            if (SpeedEnoughForJump())
            {
                jump = true;
            }
            else
            {
                if (!InversionIsLocked())
                {
                    InverseMovingDirection();
                }
            }
        }

        return jump;
    }

    private bool SpeedEnoughForJump()
    {
        return Mathf.Abs(speed) >= MINIMUM_SPEED_TO_JUMP;
    }

    protected override bool IsGrounded()
    {
        return Physics2D.Raycast(GetStartRaycastPoint(startRaycastPoint * transform.localScale.x) , rayCastDirection, groundLayer);
    }
}

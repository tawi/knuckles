﻿using UnityEngine;

public abstract class EnemyGround : EnemyBase
{
    private const float MINIMUM_DISTANCE_TO_TARGET = 40f;
    protected const float BOUNCE_RATE = 0.6f;
    protected const float LOCK_INVERSION_TIME = 0.6f;

    private Vector2 forceDirection = new Vector3(4f, 5f);
    protected float shift = 0.35f;
    protected float force = 80;
    protected float previuousBounceTime;
    protected int playerLayer;
    protected int groundLayer;
    protected float previousLockInversionTime;

    protected Vector3 startRaycastPoint;
    protected Vector3 rayCastDirection;

    protected override void Awake()
    {
        base.Awake();
        shift += GetComponent<Collider2D>().bounds.extents.x;
        playerLayer = 1 << LayerMask.NameToLayer(ConstantsSettings.PlayerMask);
        groundLayer = 1 << LayerMask.NameToLayer(ConstantsSettings.GroundMask);
        startRaycastPoint = new Vector3(-shift, -1);
        rayCastDirection = new Vector3(-1f, -1f);
    }

    protected override void FixedUpdate()
    {
        if (health.Alive)
        {
            CheckGround();

            CheckPlayerHit();

            if (ShouldJump())
                Jump();
        }
    }

    protected virtual void CheckPlayerHit()
    {
        Debug.DrawRay(GetStartRaycastPoint(startRaycastPoint), rayCastDirection * 1f, Color.black);

        var hit = PlayerHitRaycast();

        if (hit)
        {
            PlayerHit(hit);
        }
        else
        {
            Moving();
        }
    }

    private void Moving()
    {
        if (CanBounce())
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
    }

    private void BounceAway(HealthBase health)
    {
        previuousBounceTime = Time.time;
        health.TakeDamage(1);

        rb.AddForce(forceDirection * force, ForceMode2D.Impulse);
    }

    private void PlayerHit(RaycastHit2D hit)
    {
        var player = hit.transform.GetComponent<Player>();

        if (player != null)
        {
            if (CanBounce())
            {
                BounceAway(hit.transform.GetComponent<HealthBase>());
            }
        }
    }

    private bool CanBounce()
    {
        float distance = Vector2.Distance(transform.position, PlayerSpawner.Instance.Player.transform.position);

        return ((Time.time > previuousBounceTime + BOUNCE_RATE) && (distance < MINIMUM_DISTANCE_TO_TARGET));
    }

    protected virtual RaycastHit2D PlayerHitRaycast()
    {
        return Physics2D.Raycast(GetStartRaycastPoint(startRaycastPoint), rayCastDirection, 1f, playerLayer);
    }

    protected bool InversionIsLocked()
    {
        return !(Time.time > previousLockInversionTime + LOCK_INVERSION_TIME);
    }

    protected void InverseMovingDirection()
    {
        previousLockInversionTime = Time.time;
        transform.localScale = new Vector3(-1 * transform.localScale.x, transform.localScale.y, transform.localScale.z);
        speed = -1 * speed;
    }

    protected abstract bool ShouldJump();
    protected abstract bool IsGrounded();
}

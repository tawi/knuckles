﻿using UnityEngine;

public class EnemyFlyMelee : EnemyFly
{   
    protected const float HIT_FORCE = 400f;
    protected const float HIT_TIME_DELAY = 0.8f;
    private const float MULTIPLIER_SPEED = 1.4f;
    private const float COLLISION_FORCE = 20f;
    private const float BOUNCE_RATE = 0.5f;
    private const string ANIMATOR_SPEED_KEY = "Vel";

    protected float rayCastShift;
    protected float previuousHitTime;
    private float previuousBounceTime;
    private int playerLayer;
    private bool isChasing = false;
    private Vector2 bounceDirection = new Vector2(1f, 0.1f);
    private Vector3 startRaycastPoint;
    private Animator animator;
    private UnityChan.SpringManager springManager;
    private PlayerHealth targetHealth;   

    protected override void Action()
    {
        isChasing = transform.position.x > PlayerSpawner.Instance.Player.transform.position.x && targetHealth.Alive;
    }

    protected override void Awake()
    {
        base.Awake();
        rayCastShift = GetComponent<Collider2D>().bounds.extents.x + 0.35f;
        animator = GetComponent<Animator>();
        springManager = GetComponent<UnityChan.SpringManager>();
        targetHealth = PlayerSpawner.Instance.Player.transform.GetComponent<PlayerHealth>();
        springManager.enabled = false;
        playerLayer = 1 << LayerMask.NameToLayer(ConstantsSettings.PlayerMask);
        startRaycastPoint = new Vector3(-rayCastShift, -1);
    }

    protected override void FixedUpdate()
    {
        if (health.Alive)
        {
            CheckPlayerHit();
        }

        animator.SetFloat(ANIMATOR_SPEED_KEY, isChasing ? MULTIPLIER_SPEED : 1f);

        CheckDistanceForAction();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == ConstantsSettings.GroundTag)
        {
            if (CanBounce())
            {
                previuousBounceTime = Time.time;
                rb.AddForce(Vector2.up * COLLISION_FORCE, ForceMode2D.Impulse);
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == ConstantsSettings.GroundTag)
        {
            if (!IsInvoking("InvokeStopSpeed"))
                Invoke("InvokeStopSpeed", 0.5f);
        }
    }

    protected virtual void CheckPlayerHit()
    {
        Vector2 direction = (PlayerSpawner.Instance.Player.transform.position - transform.position).normalized;

        Debug.DrawRay(GetStartRaycastPoint(startRaycastPoint), direction * 1f, Color.blue);

        var hit = HitRaycast(direction);

        if (hit)
        {
            HitPlayer(hit);
        }
        else
        {
            Moving(direction);
        }
    }

    private void HitPlayer(RaycastHit2D hit)
    {
        var player = hit.transform.GetComponent<Player>();

        if (player != null)
        {
            if (CanFly())
            {
                previuousHitTime = Time.time;
                hit.transform.GetComponent<HealthBase>().TakeDamage(1);

                rb.AddForce(bounceDirection * HIT_FORCE, ForceMode2D.Impulse);
            }
        }
    }

    private void Moving(Vector2 direction)
    {
        if (CanFly())
        {
            animator.enabled = true;
            springManager.enabled = false;

            rb.velocity = isChasing ? -direction * speed * MULTIPLIER_SPEED : new Vector2(speed, rb.velocity.y);
        }
        else
        {
            animator.enabled = false;
            springManager.enabled = true;
        }
    }

    private bool CanFly()
    {
        return Time.time > previuousHitTime + HIT_TIME_DELAY;
    }

    protected virtual RaycastHit2D HitRaycast(Vector2 direction)
    {
        return Physics2D.Raycast(GetStartRaycastPoint(startRaycastPoint), direction, 1f, playerLayer);
    }    

    private bool CanBounce()
    {
        return (Time.time > previuousBounceTime + BOUNCE_RATE);
    } 

    private void InvokeStopSpeed()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }
}

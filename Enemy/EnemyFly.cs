﻿using UnityEngine;

public abstract class EnemyFly : EnemyBase
{
    [SerializeField]
    protected float minimumDistance = 35f;

    protected override void FixedUpdate()
    {
        if (health.Alive)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }

        CheckDistanceForAction();
    }

    protected void CheckDistanceForAction()
    {
        if (transform.position.x > PlayerSpawner.Instance.Player.transform.position.x)
        {
            float distance = Vector2.Distance(transform.position, PlayerSpawner.Instance.Player.transform.position);

            if (distance < minimumDistance)
            {
                Action();
            }
        }
    }

    protected abstract void Action();
}

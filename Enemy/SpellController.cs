﻿using UnityEngine;

public class SpellController : BulletBase
{
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);

        var player = collision.transform.GetComponent<Player>();

        if (player != null)
        {
            collision.transform.GetComponent<HealthBase>().TakeDamage(1);
            Destroy(gameObject);
        }  
    }
}

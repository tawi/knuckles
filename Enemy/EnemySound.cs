﻿using UnityEngine;

public class EnemySound : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] deathClips;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Death()
    {
        if (audioSource)
        {
            audioSource.clip = deathClips[Random.Range(0, deathClips.Length)];
            audioSource.Play();
        }
    }
}

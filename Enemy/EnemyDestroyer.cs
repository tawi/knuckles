﻿using UnityEngine;

public class EnemyDestroyer : DestroyerBase
{
    private const float MAX_X_OFFSET_FROM_TARGET = -35f;

    protected override void Death()
    {
        gameObject.SetActive(false);
    }

    protected override bool IsDead()
    {
        return transform.position.x <= (PlayerSpawner.Instance.Player.transform.position.x + MAX_X_OFFSET_FROM_TARGET);
    }
}

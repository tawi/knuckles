﻿using UnityEngine;

public class EnemyHealth : HealthBase
{
    private Ragdoll ragdoll;
    private EnemySound enemySound;

    protected override void Awake()
    {
        base.Awake();
        ragdoll = GetComponent<Ragdoll>();
        enemySound = GetComponent<EnemySound>();
    }

    protected override void Death()
    {
        if (enemySound)
        {
            enemySound.Death();
        }

        if (!ragdoll)
        {
            gameObject.SetActive(false);
        }
        else
        {
            ragdoll.Activate();
        }
    }
}

﻿using UnityEngine;

public class EnemyJumper : EnemyGround
{
    private float Y_RAYCAST_OFFSET = 0.35f;
    private const string ANIMATOR_GROUNDED_KEY = "IsGrounded";
    private const string ANIMATOR_VELOCITY_KEY = "Vel";

    private Vector3 playerRaycastStartPoint1;
    private Vector3 playerRaycastStartPoint2;
    private Vector3 playerRaycastDirection1;
    private Vector3 playerRaycastDirection2;
    private Animator animator;

    protected override void Awake()
    {
        base.Awake();

        animator = GetComponent<Animator>();
        Y_RAYCAST_OFFSET += GetComponent<Collider2D>().bounds.extents.y;

        startRaycastPoint = new Vector3(-shift, 0);
        rayCastDirection = new Vector3(0f, -16f);

        playerRaycastStartPoint1 = new Vector3(-shift, -1);
        playerRaycastStartPoint2 = new Vector3(-1, -Y_RAYCAST_OFFSET);
        playerRaycastDirection1 = new Vector3(-1f, -1f);
        playerRaycastDirection2 = new Vector3(0f, -1f);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        animator.SetBool(ANIMATOR_GROUNDED_KEY, isGrounded);
        animator.SetFloat(ANIMATOR_VELOCITY_KEY, rb.velocity.y > 0 ? 1 : -1);
    }

    protected override bool ShouldJump()
    {
        if (!IsGrounded())
        {
            if (!InversionIsLocked())
            {
                InverseMovingDirection();
            }
        }

        return true;
    }

    protected override bool IsGrounded()
    {
        return Physics2D.Raycast(GetStartRaycastPoint(startRaycastPoint * transform.localScale.x) , rayCastDirection, groundLayer);
    }

    protected override RaycastHit2D PlayerHitRaycast()
    {
        var hit = Physics2D.Raycast(GetStartRaycastPoint(playerRaycastStartPoint1), playerRaycastDirection1, 1f, playerLayer);

        if (!hit)
        {
            Debug.DrawRay(GetStartRaycastPoint(playerRaycastStartPoint2), playerRaycastDirection2, Color.cyan);
            hit = Physics2D.Raycast(GetStartRaycastPoint(playerRaycastStartPoint2), playerRaycastDirection2, 1f, playerLayer);
        }
        return hit;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    [SerializeField]
    private float force = 45f;
    [SerializeField]
    private Rigidbody2D[] forcePoints;
    [SerializeField]
    private Vector2 direction = Vector2.right;
    [SerializeField]
    private bool disableHinge = true;

    private List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();
    private List<Vector2> positions = new List<Vector2>();
    private List<Quaternion> rotations = new List<Quaternion>();

    private HingeJoint2D[] hinge;
    private Animator animator;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        var childRbs = GetComponentsInChildren<Rigidbody2D>();
        hinge = GetComponentsInChildren<HingeJoint2D>();

        foreach (var child in childRbs)
        {
            if (child != rb)
            {
                rigidbodies.Add(child);
                positions.Add(child.transform.localPosition);
                rotations.Add(child.transform.localRotation);
            }
        }

        if (disableHinge)
        {
            for (int i = 0; i < hinge.Length; i++)
            {
                hinge[i].enabled = false;
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void Reset()
    {
        SwitchRagdollState(false);
    }

    public void SwitchRagdollState(bool state)
    {
        if (animator)
        {
            rb.simulated = !state;
            animator.enabled = !state;
        }

        transform.localScale = new Vector3(1, 1, 1);

        for (int i = 0; i < rigidbodies.Count; i++)
        {
            rigidbodies[i].simulated = state;

            if (!state)
            {
                rigidbodies[i].transform.localPosition = positions[i];
                rigidbodies[i].transform.localRotation = rotations[i];
            }
        }

        if (hinge != null)
        {
            if (hinge.Length > 0)
                for (int i = 0; i < hinge.Length; i++)
                {
                    hinge[i].enabled = state;
                }
        }
    }

    public void Activate()
    {
        SwitchRagdollState(true);

        forcePoints[0].AddForce(direction * force * 2, ForceMode2D.Impulse);
        if (forcePoints.Length > 1)
            forcePoints[1].AddForce(direction * force, ForceMode2D.Impulse);
    }
}

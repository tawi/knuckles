﻿using UnityEngine;

public class EnemyFlyShooter : EnemyFly
{
    private const float FIRE_RATE = 4f;
    private const float CORRECTION_TIME = 0.2f;
    private const string ANIMATOR_ATTACK_KEY = "Attack";

    [SerializeField]
    private GameObject spellPrefab;
    [SerializeField]
    private Transform shotPoint;

    private float previuousLockTime;   
    private Animator animator;

    protected override void Awake()
    {
        base.Awake();

        animator = GetComponent<Animator>();
    }

    protected override void FixedUpdate()
    {
        if (health.Alive)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }

        CheckDistanceForAction();
    }

    protected override void Action()
    {
        if (CanShoot())
        {
            animator.SetTrigger(ANIMATOR_ATTACK_KEY);

            previuousLockTime = Time.time + FIRE_RATE;
        }
    }

    private bool CanShoot()
    {
        return Time.time > previuousLockTime + FIRE_RATE;
    }

    private void Shot()
    {
        var spell = Instantiate(spellPrefab, shotPoint.position, Quaternion.identity, null).GetComponent<SpellController>();

        Vector2 direction = CalculateShotDirection(spell.Speed);

        spell.SetDirection(direction);
    }

    private Vector2 CalculateShotDirection(float speed)
    {
        Vector3 startPosition = shotPoint.position;

        float currentDistance = Vector2.Distance(PlayerSpawner.Instance.Player.transform.position, startPosition);

        float time = currentDistance / speed - CORRECTION_TIME;

        Vector3 predictedPosition = PlayerSpawner.Instance.Player.transform.position + new Vector3(PlayerSpawner.Instance.Player.transform.GetComponent<Player>().Speed * time, 0);

        Vector2 direction = (predictedPosition - startPosition).normalized;

        return direction;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
{
    private const string SIGN_IN = "Please sign in";
    private const int SCORE_PER_SECOND = 1;
    public const int DEATH_SCORE = 3;

    [SerializeField]
    private Text scoreValue;
    [SerializeField]
    private Text scoreMenuValue;
    [SerializeField]
    private Text highScoreMenuValue;
    [SerializeField]
    private TextMesh menuScore;

    private float currentTime;
    private bool gameEnabled = false;

    public int HighScore { get; private set; }
    public int TotalScore { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        HighScore = 0;

        if (PlayerPrefs.HasKey("highScore"))
        {
            HighScore = PlayerPrefs.GetInt("highScore");
        }

        menuScore.text = HighScore.ToString();
        highScoreMenuValue.text = HighScore.ToString();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnFinish += MatchController_OnFinish;
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnFinish -= MatchController_OnFinish;
        MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    private void FixedUpdate()
    {
        if (PlayerSpawner.Instance.Player.PlayerHealth.Alive)
        {
            currentTime += Time.fixedDeltaTime;

            if (currentTime >= 1f)
            {
                currentTime = 0;
                TotalScore += SCORE_PER_SECOND;
                UpdateTextScore();
            }
        }
    }

    [ContextMenu("DeleteScore")]
    public void Delete()
    {
        if (PlayerPrefs.HasKey("highScore"))
        {
            PlayerPrefs.DeleteKey("highScore");
        }
    }

    private void UpdateTextScore()
    {
        scoreValue.text = TotalScore.ToString();
        scoreMenuValue.text = TotalScore.ToString();
    }

    public void SignInMessage()
    {
        menuScore.text = SIGN_IN;
        menuScore.fontSize = 38;

        if (IsInvoking("UpdateMenuScore"))
            CancelInvoke("UpdateMenuScore");

        Invoke("UpdateMenuScore", 4f);
    }

    private void UpdateMenuScore()
    {
        menuScore.fontSize = 50;
        menuScore.text = HighScore.ToString();
    }

    public void AddScore(int count)
    {
        if (gameEnabled)
        {
            TotalScore += count;
            UpdateTextScore();
        }
    }

    private void MatchController_OnFinish()
    {
        gameEnabled = false;

        if (HighScore < TotalScore)
        {
            HighScore = TotalScore;
            PlayerPrefs.SetInt("highScore", HighScore);
            PlayerPrefs.Save();
            menuScore.text = HighScore.ToString();
            highScoreMenuValue.text = HighScore.ToString();

            GoogleScoreManager.AddScore(HighScore);
        }
    }

    private void MatchController_OnReset(bool isStartedGame)
    {
        TotalScore = 0;
        currentTime = 0;
        gameEnabled = true;
        UpdateTextScore();
    }
}

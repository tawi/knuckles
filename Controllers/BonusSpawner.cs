﻿using UnityEngine;

public class BonusSpawner : MonoBehaviour
{
    [SerializeField]
    private PowerUp[] powerupPrefabs;

    public void Spawn(PlatformCreator platform)
    {
        int index = Random.Range(0, powerupPrefabs.Length);
        //int index = 0;

        var prefab = powerupPrefabs[index];

        float yPosition = platform.Left.y + platform.Offset + prefab.GetComponent<Collider2D>().bounds.extents.x;

        Vector3 position = Vector3.zero;

        if (index != 0)
        {
            position = new Vector3(Random.Range(platform.Left.x, platform.Right.x), yPosition);
        }
        else
        {
            int check = Random.Range(0, 2);

            if (check == 0)
                position = new Vector3(Random.Range(platform.Left.x, platform.transform.position.x - 3f), yPosition);
            else
                position = new Vector3(Random.Range(platform.Right.x - 0.8f, platform.Right.x), yPosition);
        }

        Instantiate(prefab.gameObject, position, Quaternion.identity, null);
    }
}

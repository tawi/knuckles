﻿using UnityEngine;
using System.Linq;

public class EnemySpawner : SingletonMonoBehaviour<EnemySpawner>
{
    private const float MIN_Y_POSITION = 4f;
    private const float MIN_Y_RANDOM_RANGE = 4f;
    private const float MAX_Y_RANDOM_RANGE = 7f;

    private int enemyCountMax;

    private NpcPool npcPool;

    protected override void Awake()
    {
        base.Awake();
        npcPool = FindObjectsOfType<NpcPool>().Where(pool => pool.NpcPoolType == NpcPool.PoolType.common).FirstOrDefault();
    }

    public void Spawn(PlatformCreator platform, Complication complication)
    {
        enemyCountMax = complication.Count;

        for (int i = 0; i < enemyCountMax; i++)
        {
            var npc = npcPool.GetNpc();

            var enemy = npc.GetComponent<EnemyBase>();

            Vector3 position = GetSpawnPosition(platform, enemy);

            npc.transform.position = position;
            enemy.Initialize(complication.NpcSpeed, complication.Hp);
        }
    }

    private Vector3 GetSpawnPosition(PlatformCreator platform, EnemyBase npc)
    {
        float yPosition = MIN_Y_POSITION;

        if (npc.Type == EnemyType.air)
        {
            yPosition += Random.Range(MIN_Y_RANDOM_RANGE, MAX_Y_RANDOM_RANGE);

            if ((yPosition + platform.Left.y) > ObjectManager.Instance.MaxYPosition)
            {
                yPosition = 5 + ObjectManager.Instance.MaxYPosition;
            }
            else
            {
                yPosition += platform.Left.y;
            }
        }
        else
        {
            yPosition += platform.Left.y;
        }

        Vector3 position = new Vector3(Random.Range(platform.Left.x, platform.Right.x), yPosition);

        return position;
    }
}

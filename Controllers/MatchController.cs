﻿using System;
using UnityEngine;

public class MatchController : SingletonMonoBehaviour<MatchController>
{
    private bool playedGame = false;

    public event Action OnFinish = delegate { };
    public event Action<bool> OnReset = delegate { };
    public event Action OnExitToMenu = delegate { };
    public event Action OnContinueReset = delegate { };  

    private void OnDisable()
    {
        if (PlayerSpawner.InstanceIfExist)
            PlayerSpawner.Instance.Player.PlayerHealth.OnDeath -= PlayerHealth_OnDeath;
    }
    
    public void Reset(bool isStartGame)
    {
        OnReset(isStartGame);
        playedGame = false;
    }   

    public void ResetToMenu()
    {
        OnExitToMenu();
        playedGame = false;
    }

    public void OnResetClick()
    {
        Reset(false);
    }

    public void SubscribeForPlayer(Player player)
    {
        player.GetComponent<PlayerHealth>().OnDeath += PlayerHealth_OnDeath;
    }

    public void OnContinueClick()
    {
#if UNITY_EDITOR||UNITY_STANDALONE
        ContinueReset();
#else
        if (adsController)
            adsController.OnRewardClick(OnContinueSuccess);
        playedGame = true;
#endif
    }

    private void OnContinueSuccess(bool success)
    {
        if (success)
        {
            ContinueReset();
        }
        else
        {
            Reset(false);
        }
    }

    private void ContinueReset()
    {
        OnContinueReset();
        Vector2 position = PlayerSpawner.Instance.Player.ContinueReset();
        FairyLaunch.Instance.Activate(position);
    }

    private void PlayerHealth_OnDeath()
    {
        OnFinish();

        if (!playedGame)
        {
            //pauseMenu.EnableResetContinueButton();
        }
    }
}

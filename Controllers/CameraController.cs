﻿using UnityEngine;

public class CameraController : SingletonMonoBehaviour<CameraController>
{
    private const float TARGET_SIZE = 11.94f;

    [SerializeField]
    private float offsetX, offsetY;

    private float t;
    private float startSize;
    private Vector3 startPosition;

    public bool ShouldFollow { get; set; }

    protected override void Awake()
    {
        base.Awake();
        startPosition = transform.position;
        startSize = Camera.main.orthographicSize;
    }

    private void OnEnable()
    {
        MatchController.Instance.OnExitToMenu += MatchController_OnExitToMenu;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
        {
            MatchController.Instance.OnExitToMenu -= MatchController_OnExitToMenu;
        }
    }

    private void FixedUpdate()
    {
        if (ShouldFollow)
        {
            if (t < 25)
            {
                t += Time.fixedDeltaTime * 3;
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, TARGET_SIZE, t * Time.fixedDeltaTime);
                transform.position = Vector3.Lerp(transform.position, new Vector3(PlayerSpawner.Instance.Player.transform.position.x + offsetX, offsetY, transform.position.z), t * Time.fixedDeltaTime);
            }
            else
            {
                transform.position = new Vector3(PlayerSpawner.Instance.Player.transform.position.x + offsetX, offsetY, transform.position.z);
            }
        }
    }

    private void MatchController_OnExitToMenu()
    {
        ShouldFollow = false;
        transform.position = startPosition;
        t = 0;
        Camera.main.orthographicSize = startSize;
    }
}

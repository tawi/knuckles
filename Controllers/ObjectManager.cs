﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectManager : SingletonMonoBehaviour<ObjectManager>
{
    private const int INITIAL_PLATFORM_COUNT = 5;
    private const float MINIMUM_Y_OFFSET = -7f;
    private const float maxY = 5f;
    private const float maxYOffset = 4f;

    private const int MIN_LENGTH = 5;
    private const int MAX_LENGTH = 15;

    private const float minimumDistance = 10f;

    [SerializeField]
    private Transform generatePoint;
    [SerializeField]
    private Transform poolParent;
    [SerializeField]
    private GameObject platformPrefab;
    [SerializeField]
    private Sprite[] furnitureSprites;

    private List<PlatformCreator> pool = new List<PlatformCreator>();
    private List<PlatformCreator> usedPlatforms = new List<PlatformCreator>();

    private float maxDistance = 12f;
    private float cubeLength;
    private Vector3 startPosition;

    private BonusSpawner bonusSpawner;
    private LevelComplication levelComplication;
    private Complication lastComplication;

    public float MaxYPosition
    {
        get
        {
            return maxY;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        cubeLength = platformPrefab.GetComponent<SpriteRenderer>().bounds.size.x;

        GameObject startPlatform = GameObject.FindGameObjectWithTag("Respawn");
        startPosition = startPlatform.transform.position + new Vector3(startPlatform.GetComponent<Collider2D>().bounds.extents.x + cubeLength / 2, 0);
        bonusSpawner = GetComponent<BonusSpawner>();
        levelComplication = GetComponent<LevelComplication>();

        GeneratePool();
        GenerateStart();
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
    }

    private void FixedUpdate()
    {
        if (usedPlatforms[usedPlatforms.Count - 1].transform.position.x < generatePoint.position.x)
        {
            HidePlatform();
            AddPlaform(true);
        }
    }

    private void GeneratePool()
    {
        for (int i = MIN_LENGTH; i < MAX_LENGTH; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                CreatePlaform(i);
            }
        }
    }

    private void CreatePlaform(int lenght)
    {
        var platform = new GameObject("Platform").AddComponent<PlatformCreator>();
        platform.transform.parent = poolParent;
        platform.PlatformLength = lenght;
        platform.Initialize(platformPrefab, furnitureSprites);
        platform.Generate();
        pool.Add(platform);
        platform.gameObject.SetActive(false);
    }

    private void GenerateStart()
    {
        for (int i = 0; i < INITIAL_PLATFORM_COUNT; i++)
        {
            AddPlaform(false);
        }
    }

    private void AddPlaform(bool sholdSpawnEnemies)
    {
        int length = MAX_LENGTH - 1;

        var complication = levelComplication.GetComplication(ScoreManager.Instance.TotalScore, LevelComplication.WaveType.common);

        if (usedPlatforms.Count > 0)
        {
            length = Random.Range(MIN_LENGTH, MAX_LENGTH);
        }

        var platform = GetPlatform(length);

        if (complication.MaxDistance > 0)
            maxDistance = complication.MaxDistance;

        if (usedPlatforms.Count > 0)
        {
            float xOffset = Random.Range(minimumDistance, maxDistance);
            float yOffset = Random.Range(-maxYOffset, maxYOffset);

            Vector3 startPosition = usedPlatforms[usedPlatforms.Count - 1].transform.position + usedPlatforms[usedPlatforms.Count - 1].RightOffset;
            ShiftPlatform(startPosition, new Vector2(xOffset, yOffset), platform);
        }
        else
        {
            ShiftPlatform(startPosition, Vector2.zero, platform);
        }

        usedPlatforms.Add(platform);
        platform.gameObject.SetActive(true);

        if (sholdSpawnEnemies)
        {
            EnemySpawner.Instance.Spawn(platform, complication);
            bonusSpawner.Spawn(platform);
        }
    }

    private void ShiftPlatform(Vector3 startPosition, Vector3 offset, PlatformCreator platform)
    {
        platform.transform.position = startPosition + offset + platform.LeftOffset;

        platform.transform.position = new Vector3(platform.transform.position.x, Mathf.Clamp(platform.transform.position.y, MINIMUM_Y_OFFSET, maxY), 0);
    }

    private PlatformCreator GetPlatform(int length)
    {
        return pool.FirstOrDefault(platform => platform.PlatformLength == length && platform.gameObject.activeSelf == false);
    }

    private void HidePlatform()
    {
        usedPlatforms[0].gameObject.SetActive(false);
        usedPlatforms.RemoveAt(0);
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        foreach (PlatformCreator platform in usedPlatforms)
        {
            platform.gameObject.SetActive(false);
        }
        usedPlatforms.Clear();
        GenerateStart();
        lastComplication = levelComplication.GetComplication(0);
    }
}

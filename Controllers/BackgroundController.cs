﻿using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    private const float MAX_DISTANCE = 20f;
    private const float RATIO = 20f;

    [SerializeField]
    private float speed = -1f;
    [SerializeField]
    private SpriteRenderer[] sprites;
    [SerializeField]
    private bool chasePlayer = false;
    [SerializeField]
    private bool ignorePlayerSpeed = false;

    private Vector3[] positions;
    private float shift;
    private int index;

    private Transform target;
    private Rigidbody2D targetSpeed;

    protected void Awake()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>();
        shift = sprites[0].bounds.size.x;
        target = Camera.main.transform;
        index = sprites.Length - 1;
        positions = new Vector3[sprites.Length];
        targetSpeed = PlayerSpawner.Instance.Player.GetComponent<Rigidbody2D>();

        for (int i = 0; i < sprites.Length; i++)
        {
            positions[i] = sprites[i].transform.position;
        }
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
        MatchController.Instance.OnExitToMenu += MatchController_OnExitToMenu;
    }

    private void OnDisable()
    {
        if (MatchController.InstanceIfExist)
        {
            MatchController.Instance.OnReset -= MatchController_OnReset;
            MatchController.Instance.OnExitToMenu -= MatchController_OnExitToMenu;
        }
    }

    private void FixedUpdate()
    {
        if (!chasePlayer)
            transform.position += new Vector3(speed * (targetSpeed.velocity.x / RATIO) * Time.fixedDeltaTime, 0);
        else if (targetSpeed.velocity.x > 1f || ignorePlayerSpeed)
            transform.position += new Vector3((targetSpeed.velocity.x + speed) * Time.fixedDeltaTime, 0);

        if (Vector2.Distance(sprites[index].transform.position, target.position) < MAX_DISTANCE)
        {
            int prevIndex = index;

            index++;

            if (index >= sprites.Length)
            {
                index = 0;
            }

            sprites[index].transform.position = sprites[prevIndex].transform.position + new Vector3(shift, 0);
        }
    }

    private void OnValidate()
    {
        shift = sprites[0].bounds.size.x;

        for (int i = 0; i < sprites.Length; i++)
        {
            if (i > 0)
                sprites[i].transform.localPosition = sprites[0].transform.localPosition + new Vector3(i * shift, 0);
        }
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (!isStartGame)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].transform.position = positions[i];
            }

            index = sprites.Length - 1;
        }
    }

    private void MatchController_OnExitToMenu()
    {
        MatchController_OnReset(false);
    }
}

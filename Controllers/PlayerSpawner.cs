﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : SingletonMonoBehaviour<PlayerSpawner>
{
    [SerializeField] Player player;

    private Vector3 platformSpawnPosition;
    private Vector3 menuSpawnPosition;

    public Player Player { get => player; }

    protected override void Awake()
    {
        base.Awake();
        GameObject startPlatform = GameObject.FindGameObjectWithTag("Respawn");
        platformSpawnPosition = startPlatform.transform.position + new Vector3(startPlatform.GetComponent<Collider2D>().bounds.extents.x + 6f, 0.9f);
        menuSpawnPosition = player.transform.position;
        MatchController.Instance.SubscribeForPlayer(player);
    }

    private void OnEnable()
    {
        MatchController.Instance.OnReset += MatchController_OnReset;
        MatchController.Instance.OnExitToMenu += MatchController_OnExitToMenu;
    }

    private void OnDisable()
    {
        MatchController.Instance.OnReset -= MatchController_OnReset;
        MatchController.Instance.OnExitToMenu -= MatchController_OnExitToMenu;
    }

    public Vector2 Reset()
    {
        Player.Reset();
        Player.transform.position = platformSpawnPosition;

        return platformSpawnPosition;
    }

    private void MatchController_OnReset(bool isStartGame)
    {
        if (isStartGame)
        {
            Player.GetComponent<Animator>().SetBool("Moving", true);
            Player.PlayerHealth.Reset();
            Player.GetComponent<PlayerSound>().SayDewayPhrase();
            Player.GetComponent<PlayerSound>().IdleTank(false);
        }
    }

    private void MatchController_OnExitToMenu()
    {
        Player.GetComponent<Animator>().SetBool("IsGrounded", true);
        Player.GetComponent<Animator>().SetBool("Moving", false);
        Player.transform.position = menuSpawnPosition;
        Player.GetComponent<HealthBase>().Alive = false;
        Player.GetComponent<PlayerSound>().IdleTank(true);
        Player.GetComponent<PlayerRagdoll>().SwitchState(true);
    }
}
